using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Ship
{
    // Start is called before the first frame update
    void Start()
    {
        Speed = 10f;
        TimeBetweenBullets = 0.5f;
    }

    protected override void Movement()
    {
        var axisX = Input.GetAxis("Horizontal");
        //Debug.Log(axisX);
        var axisY = Input.GetAxis("Vertical");
        //Debug.Log(axisY);

        transform.position = new Vector3(transform.position.x + axisX * Speed * Time.deltaTime, transform.position.y + axisY * Speed * Time.deltaTime, transform.position.z);
    }

    protected override void Shoot()
    {
        if (Input.GetKey(KeyCode.Space) && _bulletState == BulletState.BulletIn)
        {
            _bullet.transform.position = transform.position;
            Instantiate(_bullet);
            _bulletState = BulletState.Cooldown;
        }
        else if (_bulletState == BulletState.Cooldown)
        {
            _timeSinceLastBullet += Time.deltaTime;
            if (_timeSinceLastBullet >= TimeBetweenBullets)
            {
                _bulletState = BulletState.BulletIn;
                _timeSinceLastBullet = 0f;
            }
        }
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            Destroy(gameObject);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {}
}
