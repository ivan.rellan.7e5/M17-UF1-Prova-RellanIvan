using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateController : Ship
{
    
    private int _maxShots;
    private int _shotsReceived;

    // Start is called before the first frame update
    void Start()
    {
        Speed = 2f;
        Points = 20;
        TimeBetweenBullets = 2f;
        _shotsReceived = 0;
        _maxShots = 5;
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            _shotsReceived++;
            CheckVitality();
        }
    }

    void CheckVitality()
    {
        Debug.Log(_shotsReceived);
        if (_shotsReceived >= _maxShots)
        {
            GameManager.Instance.Mark += Points;
            Destroy(this.gameObject);
        }
    }
}
