using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSpawner : MonoBehaviour
{
    private enum SpawnerState
    {
        Usable,
        Used
    }

    public GameObject[] Ships;
    private float _cooldown;
    private float _timeSinceCreation;
    private SpawnerState _state;

    // Start is called before the first frame update
    void Start()
    {
        _cooldown = 2f;
        _state = SpawnerState.Usable;
    }

    // Update is called once per frame
    void Update()
    {
        CreateShip();
    }

    void CreateShip()
    {
        if (_state == SpawnerState.Usable)
        {
            int positionInArray = Random.Range(0, 2);
            int positionInX = Random.Range(-6, 6);
            GameObject newShip = Ships[positionInArray];

            newShip.transform.position = new Vector3(positionInX, transform.position.y, transform.position.z);
            Instantiate(newShip);

            _state = SpawnerState.Used;
        }
        else
        {
            _timeSinceCreation += Time.deltaTime;
            if (_timeSinceCreation >= _cooldown)
            {
                _timeSinceCreation = 0;
                _state = SpawnerState.Usable;
            }
        }
        
    }
}
