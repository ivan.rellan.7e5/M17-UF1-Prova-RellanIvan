using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ship : MonoBehaviour
{
    protected enum BulletState
    {
        BulletIn,
        BulletOut,
        Cooldown
    }

    public float Speed;
    public int Points;
    public float TimeBetweenBullets;
    protected float _timeSinceLastBullet;
    protected Camera mainCamera;
    protected BulletState _bulletState;
    [SerializeField]
    protected GameObject _bullet;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
        Points = 5;
        _bulletState = BulletState.BulletIn;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Movement();
        Shoot();
    }

    protected virtual void Movement()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - Speed * Time.deltaTime, transform.position.z);
        //if (transform.position.y >= mainCamera.orthographicSize / 2) Destroy(this.gameObject);
    }
    protected virtual void Shoot()
    {
        _timeSinceLastBullet += Time.deltaTime;
        if (_timeSinceLastBullet >= TimeBetweenBullets)
        {
            _bullet.transform.position = transform.position;
            Instantiate(_bullet);
            _timeSinceLastBullet = 0;
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            GameManager.Instance.Mark += Points;
            Destroy(this.gameObject);
        }
    }
}
